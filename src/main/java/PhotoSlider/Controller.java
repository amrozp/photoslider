package PhotoSlider;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.json.JSONObject;


public class Controller extends Application {

    private List<String> list = new ArrayList<>();
    private boolean isApplicationRunning=true;
    int j = 0;

    ImageView imageView;
    String time, url;
    URL location;




    @Override
    public void start(final Stage primaryStage) {
        try {
            Path loc = Paths.get(".");
            location = Controller.class.getProtectionDomain().getCodeSource().getLocation();
            File f = new File(location.toURI());
            /**TODO
             * trzeba jakoś automatycznie znajdować plik conf.txt
             */

            String file = readAllFile(Paths.get(f.getPath(), "conf.properties").toString());
            JSONObject json = new JSONObject(file);

            /**
             * @param time to czas z pliku conf
             * @param url to scieżka z pliku conf
             */

            time = json.getString("time");
            url = json.getString("url");

            listFilesInFolder(url);


            GridPane root = new GridPane();
            root.setAlignment(Pos.CENTER);


//            final Image images[] = new Image[list.size()];
//            for (int i = 0; i < list.size(); i++) {
//                String s ="file:Users/Piotrek/Desktop/zdjęcia/"+list.get(i);
////                images[i] = new Image("/Users/Piotrek/Desktop/zdjęcia/"+list.get(i));
//                images[i] = new Image(s);
//                //link też można niby
////                images[i] = new Image("http://sample.com/res/flower.png");
//            }

            imageView = new ImageView();

            /**
             *
             * Wątek który odpowiada za przełączanie między zdjęciami
             *
             */

            HBox hBox = new HBox();
            hBox.setSpacing(0);
            hBox.setAlignment(Pos.CENTER);
            hBox.getChildren().addAll(imageView);

            root.add(hBox, 1, 1);
            final Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setFullScreen(true);
            scene.setFill(Color.BLACK);
            Screen screen = Screen.getPrimary();

            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() {
                    Random random = new Random();
                    while (isApplicationRunning) {


                        Image img = new Image(list.get(random.nextInt( list.size())));
                        imageView.setImage(img);
                        imageView.setPreserveRatio(true);
                        imageView.fitWidthProperty().bind(primaryStage.widthProperty());
                        try {
                            Thread.sleep(Integer.valueOf(time) * 1000);
                        } catch (Exception e) {
                        }
                    }

                return null;
                }
            };
            new Thread(task).start();




            //Rectangle2D bounds = screen.getVisualBounds();


            //imageView.setFitHeight(bounds.getHeight());
           // imageView.setFitWidth(bounds.getWidth());


            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop(){
        isApplicationRunning=false;
    }

    /**
     * metoda zwraca scieżki do plików z odpowiednimi rozszerzeniami
     * @param path to scieżka do pliku z naszymi zdjęciami
     */
    public void listFilesInFolder(String path) {


        File[] files = new File(path).listFiles();


        for (File file : files) {
            if (file.isFile()) {
                if (file.getName().contains(".jpg") || file.getName().contains(".png"))
                    list.add(file.toURI().toString());
            }
        }

    }

    /**
     * @param filePath scieżka do pliku konfiguracyjnego
     * @return cały plik w postaci Stringa
     */
    private static String readAllFile(String filePath) {
        String content = "";

        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }
}